package com.academiasmoviles.homeworkfragments2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.academiasmoviles.homeworkfragments2.fragments.PrimerFragment
import com.academiasmoviles.homeworkfragments2.fragments.SegundoFragment
import com.academiasmoviles.homeworkfragments2.fragments.TercerFragment
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //TODO inicializar viewpager
        viewPager.adapter = MyTabPagerAdapter(supportFragmentManager)

        //TODO inicializar tabs
        tabLayout.setupWithViewPager(viewPager)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    //TODO crear adapter
    inner class MyTabPagerAdapter(fragmentManager: FragmentManager):
        FragmentPagerAdapter(fragmentManager){

        override fun getItem(position: Int): Fragment {
            return when(position){
                1 -> SegundoFragment()
                2 -> TercerFragment()
                else -> PrimerFragment()
            }
        }

        override fun getCount(): Int = 3

        override fun getPageTitle(position: Int): CharSequence? {
            return "Pestaña ${position+1}"
        }
    }
}